import { useState, useEffect, useContext } from 'react';
import { UsersContext } from '../../context/UsersContext';
import User from '../User/User';
import classes from './UsersList.module.css';

const UsersList = () => {
	const { users } = useContext(UsersContext);
	const [checkedUsersIds, setCheckedUsersIds] = useState([]);
	const [filteredUsers, setFilteredUsers] = useState([]);

	useEffect(() => setFilteredUsers(users), [users]);
	useEffect(() => console.log(checkedUsersIds), [checkedUsersIds]);

	const checkUsers = async e => {
		if (e.target.checked) {
			const checkedUser = users.find(user => user.id === Number(e.target.name));
			setCheckedUsersIds([...checkedUsersIds, checkedUser.id]);
		} else {
			setCheckedUsersIds(checkedUsersIds.filter(user => user !== Number(e.target.name)));
		}
	};

	const filterUsers = e => {
		setFilteredUsers(
			users.filter(
				user =>
					user.first_name.toLowerCase().includes(e.target.value.toLowerCase().trim()) ||
					user.last_name.toLowerCase().includes(e.target.value.toLowerCase().trim())
			)
		);
	};

	return (
		<>
			<input
				type="text"
				name="filter_users"
				className={classes.filter_input}
				onChange={e => filterUsers(e)}
				placeholder="search user..."
				autoComplete="off"
			/>

			<ul>
				{users.length ? (
					filteredUsers.map(user => {
						return (
							<User
								key={user.id}
								user={user}
								checkUsers={checkUsers}
								checkedUsersIds={checkedUsersIds}
							/>
						);
					})
				) : (
					<h1 className={classes.list_loading}>List loading...</h1>
				)}
			</ul>
		</>
	);
};

export default UsersList;
