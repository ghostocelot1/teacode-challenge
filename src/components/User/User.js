import classes from './User.module.css';

const User = ({ user, checkUsers, checkedUsersIds }) => {
	return (
		<>
			<label htmlFor={user.name}>
				<li className={classes.user_container}>
					<div className={classes.user_subcontainer}>
						<div
							className={`${classes.avatar_container} ${
								user.gender === 'Male' ? classes.male : classes.female
							}`}
						>
							{user.avatar ? (
								<img className={classes.avatar} src={user.avatar} alt="#" />
							) : (
								<div className={classes.img_alt}>
									{user.first_name.slice(0, 1)}
									{user.last_name.slice(0, 1)}
								</div>
							)}
						</div>
						<h3 className={user.gender === 'Male' ? classes.male_text : classes.female_text}>
							{user.first_name} {user.last_name}
						</h3>
					</div>
					<div className={classes.checkbox_container}>
						<input
							type="checkbox"
							checked={checkedUsersIds.includes(user.id)}
							name={user.id}
							onChange={e => checkUsers(e)}
						/>
					</div>
				</li>
			</label>
		</>
	);
};

export default User;
