import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import UsersContextProvider from './context/UsersContext';

ReactDOM.render(
	<React.StrictMode>
		<UsersContextProvider>
			<App />
		</UsersContextProvider>
	</React.StrictMode>,
	document.getElementById('root')
);
