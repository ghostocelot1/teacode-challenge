import { useState, useEffect, createContext } from 'react';

export const UsersContext = createContext();

const UsersContextProvider = props => {
	const [users, setUsers] = useState([]);

	useEffect(() => {
		fetch('https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json')
			.then(res => res.json())
			.then(data => {
				data.sort((a, b) => a.last_name.localeCompare(b.last_name));
				setUsers(data);
			});
	}, []);

	return (
		<UsersContext.Provider value={{ users, setUsers }}>{props.children}</UsersContext.Provider>
	);
};

export default UsersContextProvider;
