import UsersList from './components/UsersList/UsersList';

function App() {
	return (
		<div className="App">
			<UsersList />
		</div>
	);
}

export default App;
