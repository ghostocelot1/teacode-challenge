# User search app

App developed for recruitment purposes. Displays a list of users along with basic information. You can interact with the interface by typing characters in the searchbar in order to filter the list. Another functionality allows to mark particular users with checkboxes. Each check and uncheck fires a function that logs a list o currently checked users ids.

## instructions

In order to run the application on your localhost please follow the guidelines:

- download the repository
- make sure you have node.js installed on your machine
- navigate into the project folder with terminal
- install the project dependencies with 'npm install'
- run the command 'npm start' to run the app on localhost

## Live version

https://tiny-hot.surge.sh/
